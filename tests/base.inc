<?php
/**
 * @file
 * Test cases for Ultimate Lock.
 */

/**
 * Tests for Ultimate Lock.
 */
class UltimateLockWebTestCaseBase extends DrupalWebTestCase {
  use UltimateLockUnitTestCaseTraits;

  protected $service;
  protected $serviceName;
  protected $itemName = 'UltimateLockItemMemory';

  /**
   * Setup test cases.
   */
  public function setUp() {
    parent::setUp(array('ultimate_lock'));
    $this->service = UltimateLock::service(
      $this->serviceName,
      array('ItemClass' => $this->itemName)
    );
  }
}

/**
 * Common test functions for unit test cases.
 */
trait UltimateLockUnitTestCaseTraits {
  /**
   * Test acquiring of lock.
   */
  public function testAcquire() {
    $lock = $this->service->acquire('mylockname', 20);
    if (!$lock) {
      $this->fail('Lock was not acquired');
    }

    $this->assertTrue($this->service->isLocked('mylockname'), 'Acquire lock');

    $lock->release();
    $this->assertFalse($this->service->isLocked('mylockname'), 'Release lock');
  }

  /**
   * Test lock expiration.
   */
  public function testExpiration() {
    $time = microtime(TRUE);
    $lock = $this->service->acquire('mylockname', 1);
    if (!$lock) {
      $this->fail('Lock was not acquired');
    }

    $this->assertTrue($lock->getExpire() >= $time + 1, 'Expiration time ok');
    sleep(1);
    $this->assertTrue($this->service->isLocked('mylockname'), 'Lock has expired but has not been garbage collected yet');
    $this->service->garbageCollect();
    $this->assertFalse($this->service->isLocked('mylockname'), 'Lock has expired');
    $lock->release();
  }

  /**
   * Test re-acquire.
   */
  public function testReAcquire() {
    $time = microtime(TRUE);
    $lock = $this->service->acquire('mylockname', 1);
    if (!$lock) {
      $this->fail('Lock was not acquired');
    }

    $this->assertTrue($lock->getExpire() >= $time + 1, 'Expiration time ok');
    sleep(1);
    $this->assertTrue($lock->getExpire() < microtime(TRUE), 'Lock has expired');

    $time = microtime(TRUE);
    $lock->reAcquire(1);
    if (!$lock) {
      $this->fail('Lock was not re-acquired');
    }

    $this->assertTrue($lock->getExpire() >= $time + 1, 'Expiration time ok');
    sleep(1);
    $this->assertTrue($lock->getExpire() < microtime(TRUE), 'Lock has expired');

    $time = microtime(TRUE);
    $lock = $this->service->reAcquire('mylockname', 1);
    var_dump($lock);
    if (!$lock) {
      $this->fail('Lock was not re-acquired');
    }

    $this->assertTrue($lock->getExpire() >= $time + 1, 'Expiration time ok');
    sleep(1);
    $this->assertTrue($lock->getExpire() < microtime(TRUE), 'Lock has expired');

    $lock->release();
    // $lock->release();
  }

  /**
   * Test nested acquire.
   */
  public function testNested() {
    $time = microtime(TRUE);
    $lock = $this->service->acquire('mylockname', 10);
    if (!$lock) {
      $this->fail('Lock was not acquired');
    }

    $this->assertTrue($lock->getExpire() >= $time + 10, 'Expiration time ok');

    $lock2 = $this->service->acquire('mylockname', 10);
    $this->assertTrue($lock->getExpire() >= $time + 10, 'Expiration time ok');
  }

  /**
   * Test nested acquire.
   */
  public function testEventHandlers() {
    $time = microtime(TRUE);
    $lock = $this->service->acquire('mylockname', 10);
    if (!$lock) {
      $this->fail('Lock was not acquired');
    }

    $this->assertTrue($lock->getExpire() >= $time + 10, 'Expiration time ok');

    $executed = FALSE;
    $lock->addEventHandler('release', function () use (&$executed) {
      $executed = TRUE;
    });
    $lock->release();

    $this->assertTrue($executed, 'Event handler was executed');
  }
}
