<?php
/**
 * @file
 * Memory based event handler lock item for Ultimate Lock tests.
 */

use Gielfeldt\Ultimate\ShutdownHandler;

/**
 * Class for a lock item handling events via internal stack.
 */
class UltimateLockItemMemoryTest extends UltimateLockItemMemory {
  /**
   * Constructor.
   */
  public function __construct(UltimateLockServiceInterface $service) {
    $this->service = $service;
    $this->shutdown = new ShutdownHandler(
      array(get_class($this), 'destroy'),
      array(
        $this->service,
        &$this->id,
        &$this->requestId,
        &$this->persist,
      ));
  }

  /**
   * Invoke the callback with error handling.
   *
   * @param callback $callback
   *   The callback to invoke.
   * @param array $args
   *   Arguments for the callback.
   *
   * @return mixed
   *   The result from the callback.
   */
  static protected function invokeCallback($callback, $args) {
    $callback_name = NULL;
    if (ShutdownHandler::isCallable($callback, FALSE, $callback_name)) {
      return call_user_func_array($callback, $args);
    }
    else {
      throw new RuntimeException('Callback ' . $callback_name . ' not found or not callable!');
    }
  }
}
