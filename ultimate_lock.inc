<?php
/**
 * @file
 * Base stuff.
 */

use Gielfeldt\ShutdownHandler;

/**
 * Drupal-flavoured shutdown handler.
 */
class DrupalShutdownHandler extends ShutdownHandler {
  /**
   * Register Drupal shutdown handler.
   *
   * @param callback $callback
   *   Callback to call on shutdown.
   * @param array $arguments
   *   Arguments for the callback.
   */
  protected function registerShutdownFunction($callback, array $arguments = array()) {
    drupal_register_shutdown_function($callback, $arguments);
  }
}

/**
 * The pseudo-name space for UltimateLock.
 */
class UltimateLock {
  protected static $initialized;

  protected static function initialize() {
    if (!self::$initialized) {
      foreach (variable_get('lock_backends', array()) as $include) {
        require_once DRUPAL_ROOT . '/' . $include;
      }
      self::$initialized = TRUE;
    }
  }

  /**
   * Factory.
   *
   * @param string $name
   *   Name of the service.
   *
   * @return UltimateLockServiceInterface
   *   The lock service object.
   */
  public static function service($service_class, $storage_class, $item_class) {
    static::initialize();
    return new $service_class(new $storage_class($item_class));
  }

  /**
   * Get the default service.
   *
   * @return UltimateLockServiceInterface
   *   An object implementing the UltimateLockServiceInterface instance.
   */
  public static function defaultService() {
    return self::service(
      variable_get('ultimate_lock_default_service', 'UltimateLockService'),
      variable_get('ultimate_lock_default_storage', 'UltimateLockStorageDatabase'),
      variable_get('ultimate_lock_default_item', 'UltimateLockItem')
    );
  }

  /**
   * Acquire lock via default service.
   *
   * @see UltimateLockServiceInterface::acquire()
   */
  public static function acquire($name, $timeout = 30.0) {
    return self::defaultService()->acquire($name, $timeout);
  }

  /**
   * Re-acquire lock via default service.
   *
   * @see UltimateLockServiceInterface::reAcquire()
   */
  public static function reAcquire($name, $timeout = 30.0) {
    return self::defaultService()->reAcquire($name, $timeout);
  }

  /**
   * Load lock via default service.
   *
   * @see UltimateLockServiceInterface::load()
   */
  public static function load($name) {
    return self::defaultService()->load($name);
  }

  /**
   * Check lock via default service.
   *
   * @see UltimateLockServiceInterface::isLocked()
   */
  public static function isLocked($name) {
    return self::defaultService()->isLocked($name);
  }

  /**
   * Wait for lock via default service.
   *
   * @see UltimateLockServiceInterface::waitForLock()
   */
  public static function waitForLock($name) {
    return self::defaultService()->waitForLock($name);
  }

  /**
   * Garbage collect via default service.
   *
   * @see UltimateLockServiceInterface::garbageCollect()
   */
  public static function garbageCollect() {
    return self::defaultService()->garbageCollect();
  }

  /**
   * Clean up via default service.
   *
   * @see UltimateLockServiceInterface::cleanup()
   */
  public static function cleanup() {
    return self::defaultService()->cleanup();
  }
}


/**
 * ----------------------------------
 * INTERFACES
 * ----------------------------------
 */


/**
 * The interface describing a UltimateLock service.
 */
interface UltimateLockServiceInterface {
  /**
   * Initialize service.
   *
   * @param array $options
   *   Options for the service.
   */
  public function __construct(UltimateLockStorageInterface $storage);

  /**
   * Get a unique id for the current request.
   *
   * @return string
   *   Unique request id.
   */
  public function getRequestId();

  /**
   * Acquire a lock.
   *
   * This function must acquire the lock in the appropriate backend and return
   * an UltimateLockItem object.
   *
   * The function is also responsible for releasing the lock first, if it has
   * expired.
   *
   * @param string $name
   *   The name of the lock.
   * @param float $timeout
   *   The timeout of the lock.
   *
   * @return mixed
   *   UtlimateLockItem on success, FALSE if not.
   */
  public function acquire($name, $timeout = 30.0);

  /**
   * Re-acquire a lock.
   *
   * As opposed to acquire(), this function does not release the lock first,
   * if it has expired, but it instead reclaims it for the current request.
   * The lock returned keeps the lock id and event handlers associated with
   * the lock.
   *
   * If the existing lock can not be required, the function must return FALSE.
   *
   * @param string $name
   *   The name of the lock.
   * @param float $timeout
   *   The timeout of the lock.
   *
   * @return mixed
   *   UtlimateLockItem on success, FALSE if not.
   */
  public function reAcquire($name, $timeout = 30.0);

  /**
   * Release a lock.
   *
   * @param string $id
   *   The id of the lock, as provided by the UltimateLockItem object
   *   from acquire().
   * @param int $expire
   *   (optional) Only release if the lock expiration is less and the specified.
   * @param int $request_id
   *   (optional) Only release if the lock has the specified request id.
   *
   * @return boolean
   *   TRUE if release, FALSE if not.
   */
  public function release($id, $expire = NULL, $request_id = NULL);

  /**
   * Check if lock is taken.
   *
   * @param string $name
   *   Name of the lock.
   *
   * @return boolean
   *   TRUE if taken, FALSE if available.
   */
  public function isLocked($name);

  /**
   * Wait for a lock to be released.
   *
   * @param string $name
   *   The name of the lock.
   * @param int $delay
   *   The maximum number of seconds to wait, as an integer.
   *
   * @return boolean
   *   TRUE if the lock holds, FALSE if it is available.
   */
  public function waitForLock($name, $delay = 30);

}

interface UltimateLockStorageInterface {
  public function __construct($item_class);

  /**
   * Load a lock.
   *
   * Must return a persisted lock in order to avoid auto-release.
   *
   * @param string $name
   *   The name of the lock.
   *
   * @return mixed
   *   UltimateLockItemInterface on success, FALSE if not.
   */
  public function load($name);

  /**
   * Insert the lock into the storage.
   *
   * Must return a unique identifier for the lock, if successful.
   *
   * @param UltimateLockItemInterface $lock
   *   The lock object to insert.
   *
   * @return mixed
   *   ID if successful, FALSE if not.
   */
  public function insert(UltimateLockItemInterface $lock);

  /**
   * Update a current lock's expiration time.
   *
   * @param UltimateLockItem $lock
   *   The lock object to update.
   * @param string $request_id
   *   The request of the lock. The lock will only be updated, if the request id
   *   matches.
   *
   * @return boolean
   *   TRUE on success, FALSE on failure.
   */
  public function update(UltimateLockItemInterface $lock, $request_id = NULL);

  /**
   * Expire a lock.
   *
   * @param string $id
   *   The id of the lock.
   * @param float $expire
   *   (optional) Only delete the lock, if its expiration time is lesser than
   *   $expire.
   * @param string $request_id
   *   (optiona) Only delete the lock, if its request_id matches $request_id.
   *
   * @return boolean
   *   TRUE on success, FALSE on failure.
   */
  public function delete($id, $expire = NULL, $request_id = NULL);

  /**
   * Fetch a lock from the storage.
   *
   * @param string $name
   *   Name of the lock.
   *
   * @return array
   *   Associative array containing id, request_id and expire.
   */
  public function fetch($name);

  /**
   * Garbage collect expired locks.
   *
   * This method should release expired locks.
   */
  public function garbageCollect();

  /**
   * Clean up storage.
   *
   * This method should remove the expired locks from storage, in case this is
   * needed.
   */
  public function cleanup();
}

/**
 * Interface describing a lock item.
 */
interface UltimateLockItemInterface {
  /**
   * Initialize item.
   *
   * @param UltimateLockServiceInterface $service
   *   The service creating this lock item.
   */
  public function __construct(UltimateLockServiceInterface $service);

  /**
   * Get lock id.
   *
   * @return string
   *   The unique id of the lock item.
   */
  public function getId();

  /**
   * Set lock id.
   *
   * @param string $id
   *   The id to set.
   */
  public function setId($id);

  /**
   * Get lock name.
   *
   * @return string
   *   The name the lock item.
   */
  public function getName();

  /**
   * Set lock name.
   *
   * @param string $name
   *   The name to set.
   */
  public function setName($name);

  /**
   * Get lock request id.
   *
   * @return string
   *   The request id of the lock item.
   */
  public function getRequestId();

  /**
   * Set lock request id.
   *
   * @param string $request_id
   *   The request id to set.
   */
  public function setRequestId($request_id);

  /**
   * Get lock expiration.
   *
   * @return float
   *   The expiration time of the lock item.
   */
  public function getExpire();

  /**
   * Set lock expiration.
   *
   * @param float $expire
   *   The expiration time to set .
   */
  public function setExpire($expire);

  /**
   * Release the lock.
   *
   * @param float $expire
   *   (optional) If set, the lock will only be released if its expiration time
   *   is lesser than $expire.
   *
   * @return boolean
   *   TRUE if released succeeded, FALSE if not.
   */
  public function release($expire = NULL);

  /**
   * Re-acquire the lock.
   *
   * @param float $timeout
   *   The timeout of the new lock.
   *
   * @return boolean
   *   TRUE if reacquired, FALSE if not.
   */
  public function reAcquire($timeout = 30.0);

  /**
   * Persist the lock.
   *
   * @param boolean $persist
   *   Whether or not to persist the lock across requests. If FALSE, then the
   *   lock will be released during the end of the lock items scope or at the
   *   end of the script.
   */
  public function persist($persist = TRUE);

  /**
   * Check if the lock is persisted.
   *
   * @return boolean
   *   TRUE if persisted, FALSE if not.
   */
  public function isPersisted();

  /**
   * Add event handler.
   *
   * @param string $event
   *   Name of the event.
   * @param callback $callback
   *   The callback to invoke.
   * @param array $args
   *   The arguments for the callback.
   */
  public function addEventHandler($event, $callback, $args = array());
}


/**
 * ----------------------------------
 * BASE IMPLEMENTATIONS
 * ----------------------------------
 */


/**
 * Base class for UltimateLock services.
 */
abstract class UltimateLockService implements UltimateLockServiceInterface {
  /**
   * The options the service was initialized with.
   */
  protected $storage;

  /**
   * Static cache of request id for the current request.
   */
  protected static $requestId;

  /**
   * Initialize object.
   *
   * @see UltimateLockServiceInterface::__construct()
   */
  public function __construct(UltimateLockStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * Get request id.
   *
   * @see UltimateLockServiceInterface::getRequestId()
   */
  public function getRequestId() {
    if (!isset(self::$requestId)) {
      // Assign a unique id.
      self::$requestId = uniqid(mt_rand(), TRUE);
    }
    return self::$requestId;
  }

  /**
   * Acquire a lock via the database.
   *
   * @see UltimateLockServiceInterface::acquire()
   */
  public function acquire($name, $timeout = 30.0, $retry = 3) {
    if (is_null($name)) {
      throw new InvalidArgumentException('Lock name cannot be NULL!');
    }

    // Ensure that the timeout is at least 1 ms.
    $timeout = max($timeout, 0.001);

    $lock = $this->storage->load($name);
    if ($lock) {
      // A lock exists. Is it ours?
      if ($lock->getRequestId() == $this->getRequestId()) {
        // Yes it is. Let's update the expiration.
        $lock->setExpire(microtime(TRUE) + $timeout);
        if ($this->update($lock, $this->getRequestId())) {
          $lock->persist(FALSE);
          return $lock;
        }
        else {
          return FALSE;
        }
      }
      // Is it expired?
      elseif ($retry > 0 && $lock->getExpire() < microtime(TRUE)) {
        $lock->release(microtime(TRUE));
        return $this->acquire($name, $timeout, $retry - 1);
      }
      else {
        // Nope, lock failed.
        return FALSE;
      }
    }

    $expire = microtime(TRUE) + $timeout;

    $lock = $this->storage->createItemInstance();
    $lock->setName($name);
    $lock->setRequestID($this->getRequestId());
    $lock->setExpire($expire);
    $lock->persist(TRUE);

    if ($id = $this->insert($lock)) {
      $lock->setId($id);
      $lock->persist(FALSE);
      return $lock;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Re-acquire a lock.
   *
   * @see UltimateLockServiceInterface::reAcquire()
   */
  public function reAcquire($name, $timeout = 30.0) {
    if ($lock = $this->load($name)) {
      return $lock->reAcquire($timeout) ? $lock : FALSE;
    }
    return FALSE;
  }

  /**
   * Release a lock.
   *
   * @see UltimateLockServiceInterface::release()
   */
  public function release($id, $expire = NULL, $request_id = NULL) {
    return $this->delete($id, $expire, $request_id) ? TRUE : FALSE;
  }

  /**
   * Load a lock.
   *
   * @see UltimateLockServiceInterface::load()
   */
  public function load($name) {
    if ($lock = $this->fetch($name)) {
      $item = $this->getItem($lock['item']);
      $item->setId($lock['id']);
      $item->setName($name);
      $item->setRequestId($lock['request_id']);
      $item->setExpire($lock['expire']);
      // When loaded, we persist it to avoid it being released upon destroy.
      $item->persist(TRUE);
      return $item;
    }
    return FALSE;
  }

  /**
   * Check if locked is taken.
   *
   * @see UltimateLockServiceInterface::load()
   */
  public function isLocked($name) {
    $lock = $this->load($name);
    return $lock ? TRUE : FALSE;
  }

  /**
   * Wait for a lock to be available.
   *
   * This function may be called in a request that fails to acquire a desired
   * lock. This will block further execution until the lock is available or the
   * specified delay in seconds is reached. This should not be used with locks
   * that are acquired very frequently, since the lock is likely to be acquired
   * again by a different request while waiting.
   *
   * @param string $name
   *   The name of the lock.
   * @param int $delay
   *   The maximum number of seconds to wait, as an integer.
   *
   * @return boolean
   *   TRUE if the lock holds, FALSE if it is available.
   *
   * @see lock_wait()
   * @see UltimateLockServiceInterface::waitForLock()
   */
  public function waitForLock($name, $delay = 30) {
    $delay = (int) $delay * 1000000;

    // Begin sleeping at 25ms.
    $sleep = 25000;
    while ($delay > 0) {
      // This function should only be called by a request that failed to get a
      // lock, so we sleep first to give the parallel request a chance to finish
      // and release the lock.
      usleep($sleep);
      // After each sleep, increase the value of $sleep until it reaches
      // 500ms, to reduce the potential for a lock stampede.
      $delay = $delay - $sleep;
      $sleep = min(500000, $sleep + 25000, $delay);
      if (!$this->isLocked($name)) {
        // No longer need to wait.
        return FALSE;
      }
    }
    // The caller must still wait longer to get the lock.
    return TRUE;
  }

  /**
   * No garbage collection by default.
   *
   * @see UltimateLockServiceInterface::garbageCollect()
   */
  public function garbageCollect() {}

  /**
   * No clean up by default.
   *
   * @see UltimateLockServiceInterface::cleanup()
   */
  public function cleanup() {}
}

/**
 * Class for a lock item.
 */
abstract class UltimateLockItemBase implements UltimateLockItemInterface {
  /**
   * Reference counter for item objects.
   *
   * Only release the first item upon destructing, when nested locks are
   * acquired.
   *
   * @var array
   */
  protected static $counter = array();

  /**
   * The service that created this lock item.
   *
   * @var UltimateLockServiceInterface
   */
  protected $service;

  /**
   * Persistance of the lock item, i.e. release on destroy or not.
   *
   * @var boolean
   */
  protected $persist = FALSE;

  /**
   * Id of the lock item.
   *
   * @var string
   */
  protected $itemId;

  /**
   * The expiration time of the lock item in UNIX timestamp format.
   *
   * @var float
   */
  protected $expire;

  /**
   * The request id this lock item was created with.
   *
   * @var string
   */
  protected $requestId;


  /**
   * Constructor.
   *
   * @see UltimateLockItemInterface::__construct()
   */
  public function __construct(UltimateLockServiceInterface $service) {
    $this->service = $service;
    $this->shutdown = new DrupalShutdownHandler(
      array(get_class($this), 'destroy'),
      array(
        $this->service,
        &$this->itemId,
        &$this->requestId,
        &$this->persist,
      ));
  }

  /**
   * Get lock id.
   *
   * @see UltimateLockItemInterface::getId()
   */
  public function getId() {
    return $this->itemId;
  }

  /**
   * Set lock id.
   *
   * @see UltimateLockItemInterface::setId()
   */
  public function setId($id) {
    $this->itemId = $id;
    $this->shutdown->reRegister(get_class($this->service) . ':' . $id);
  }

  /**
   * Get lock name.
   *
   * @see UltimateLockItemInterface::getName()
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set lock name.
   *
   * @see UltimateLockItemInterface::setName()
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Get lock request id.
   *
   * @see UltimateLockItemInterface::getRequestId()
   */
  public function getRequestId() {
    return $this->requestId;
  }

  /**
   * Set lock request id.
   *
   * @see UltimateLockItemInterface::setRequestId()
   */
  public function setRequestId($request_id) {
    $this->requestId = $request_id;
  }

  /**
   * Get lock expiration.
   *
   * @see UltimateLockItemInterface::getExpire()
   */
  public function getExpire() {
    return $this->expire;
  }

  /**
   * Set lock expiration.
   *
   * @see UltimateLockItemInterface::setExpire()
   */
  public function setExpire($expire) {
    $this->expire = $expire;
  }

  /**
   * Release the lock.
   *
   * @see UltimateLockItemInterface::release()
   */
  public function release($expire = NULL) {
    if ($this->service->release($this->itemId, $expire)) {
      static::executeEventHandlers($this->itemId, 'release');
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Re-acquire the lock.
   *
   * @see UltimateLockItemInterface::reAcquire()
   */
  public function reAcquire($timeout = 30.0) {
    $timeout = max($timeout, 0.001);
    $this->setExpire(microtime(TRUE) + $timeout);
    $this->setRequestId($this->service->getRequestId());
    return $this->service->update($this) ? TRUE : FALSE;
  }

  /**
   * Persist the lock.
   *
   * @see UltimateLockItemInterface::persist()
   */
  public function persist($persist = TRUE) {
    $this->persist = $persist;
  }

  /**
   * Check if the lock is persisted.
   *
   * @see UltimateLockItemInterface::isPersisted()
   */
  public function isPersisted() {
    return $this->persist ? TRUE : FALSE;
  }

  /**
   * No event handlers for the base implementation.
   *
   * @see UltimateLockItemInterface::addEventHandler()
   */
  public function addEventHandler($event, $callback, $args = array()) {
    throw new RuntimeException(get_class($this) . ' does not support event handlers.');
  }

  /**
   * Execute event handlers.
   *
   * @see UltimateLockItemInterface::executeEventHandlers()
   */
  static protected function executeEventHandlers($id, $event) {}

  /**
   * Invoke the callback with error handling.
   *
   * @param callback $callback
   *   The callback to invoke.
   * @param array $args
   *   Arguments for the callback.
   *
   * @return mixed
   *   The result from the callback.
   */
  static protected function invokeCallback($callback, $args) {
    if (DrupalShutdownHandler::isCallable($callback, FALSE, $callback_name)) {
      return call_user_func_array($callback, $args);
    }
    else {
      watchdog('ultimate_lock', 'Callback %callback() not found or not callable!', array(
        '%callback' => $callback_name,
      ), WATCHDOG_ERROR);
    }
  }

  /**
   * Destructor.
   *
   * Run the shutdown handler on destruction, if applicable.
   */
  public function __destruct() {
    $this->shutdown->run();
  }

  /**
   * Real destructor.
   *
   * @param UltimateLockServiceInterface $service
   *   The lock service object.
   * @param string $id
   *   The id of the lock.
   * @param string $request_id
   *   The request id.
   * @param boolean $persist
   *   Persist lock.
   */
  static public function destroy(UltimateLockServiceInterface $service, $id, $request_id, $persist) {
    if ($id && !$persist && $service->release($id, NULL, $request_id)) {
      static::executeEventHandlers($id, 'release');
    }
  }
}
