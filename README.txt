CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Usage
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------
The Ultimate Lock module provides an OOP based framework for handling locks
in a robust, extensible and feature-rich way.


 * For a full description of the module, visit the project page:
   https://drupal.org/project/ultimate_lock

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/ultimate_lock


REQUIREMENTS
------------
 * Drupal 7
 * xautoload module


RECOMMENDED MODULES
-------------------
 * Ultimate Lock Memcache (https://www.drupal.org/project/ultimate_lock_memcache):
   Use memcache for locks.

 * Redis (https://www.drupal.org/project/redis):
   Use redis for queues.


INSTALLATION
------------
 * Install required composer packages
  * %> composer install
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * To use as a default lock backend, configure settings.php

Basic/simple configuration:
@code
 $conf['lock_inc'] = 'sites/all/modules/contrib/ultimate_lock.lock.inc';
@endcode

Adding eventhandler support through queues:
@code
 $conf['lock_inc'] = 'sites/all/modules/contrib/ultimate_lock.lock.inc';
 $conf['ultimate_lock_service_options_UltimateLockServiceDatabase'] = array(
   'ItemClass' => 'UltimateLockItemQueue',
 );
@endcode

Using memcache:
@code
 $conf['lock_inc'] = 'sites/all/modules/contrib/ultimate_lock.lock.inc';
 $conf['ultimate_lock_default_service'] = 'UltimateLockServiceMemcache';
@endcode

Using memcache with redis queues:
@code
 $conf['lock_inc'] = 'sites/all/modules/contrib/ultimate_lock.lock.inc';
 $conf['ultimate_lock_default_service'] = 'UltimateLockServiceMemcache';
 $conf['ultimate_lock_service_options_UltimateLockServiceMemcache'] = array(
   'ItemClass' => 'UltimateLockItemQueue',
 );
 $conf['ultimate_lock_item_options_UltimateLockItemQueue'] = array(
   'class' => 'DrupalRedisQueue',
 );
@endcode

USAGE
-------------
Acquire lock:
@code
 $lock = UltimateLock::service(
   'UltimateLockServiceDatabase',
   array('ItemClass' => 'UltimateLockItemQueue')
 )->acquire('mylockname', 20);
 if ($lock) {
   // We got the lock!
   $lock->addEventHandler('release', 'mycallback');
   // Lock is released when object is destroyed (out-of-scope)
   // $lock->release()
 }
@endcode

Persist lock:
@code
 $lock = UltimateLock::acquire('mylockname', 20);
 if ($lock) {
   // We got the lock! Don't release upon end of request.
   $lock->persist();
 }
@endcode

Re-acquire lock from other request:
@code
 $lock = UltimateLock::reAcquire('mylockname', 20);
 if ($lock) {
   // We got the lock! 
   // Lock is released when object is destroyed (out-of-scope)
   // $lock->release()
 }
@endcode

Creating a service handler:
 * See UltimateLockServiceInterface in ultimate_lock.inc.
 * See includes/service/database.inc for inspiration.

Creating an item handler:
 * See UltimateLockItemInterface in ultimate_lock.inc.
 * See includes/item/queue.inc for inspiration.

TROUBLESHOOTING
---------------
 * If the locks aren't working:

   - Debug! Debug like the wind!


FAQ
---
Q: My locks aren't working

A: Didn't I tell you to debug like the wind?


MAINTAINERS
-----------
Current maintainers:
 * Thomas S. Gielfeldt (gielfeldt) - https://drupal.org/user/366993

This project has been sponsored by:
 * Blood, sweat and tears, provided by gielfeldt.
