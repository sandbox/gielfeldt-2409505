<?php
/**
 * @file
 * A wrapper for using Ultimate Lock as a lock backend.
 */

// Xautoload doesn't work this early.
require_once __DIR__ . '/vendor/gielfeldt/shutdownhandler/src/ShutdownHandler.php';
require_once __DIR__ . '/ultimate_lock.inc';

/**
 * Initialize the locking system.
 */
function lock_initialize() {
  global $locks;

  $locks = array();
}

/**
 * Helper function to get this request's unique id.
 */
function _lock_id() {
  // Do not use drupal_static(). This identifier refers to the current
  // client request, and must not be changed under any circumstances
  // else the shutdown handler may fail to release our locks.
  static $lock_id;

  if (!isset($lock_id)) {
    // Assign a unique id.
    $lock_id = UltimateLock::defaultService()->getRequestId();
    // We only register a shutdown function if a lock is used.
    drupal_register_shutdown_function('lock_release_all', $lock_id);
  }
  return $lock_id;
}

/**
 * Acquire (or renew) a lock, but do not block if it fails.
 *
 *  @see UltimateLock::acquire()
 */
function lock_acquire($name, $timeout = 30.0) {
  global $locks;

  $lock = UltimateLock::acquire($name, $timeout);
  if ($lock) {
    $lock->persist(TRUE);
    $locks[$name] = $lock;
    return TRUE;
  }
  return FALSE;
}

/**
 * Check if lock acquired by a different process may be available.
 *
 * @see UltimateLock::isLocked()
 */
function lock_may_be_available($name) {
  return !UltimateLock::isLocked($name);
}

/**
 * Wait for a lock to be available.
 *
 * @see UltimateLock::waitForLock()
 */
function lock_wait($name, $delay = 30) {
  return UltimateLock::waitForLock($name, $delay);
}

/**
 * Release a lock previously acquired by lock_acquire().
 *
 * @param string $name
 *   The name of the lock.
 *
 * @see UltimateLock::release()
 */
function lock_release($name) {
  global $locks;

  if (!empty($locks[$name])) {
    $lock = $locks[$name];
    unset($locks[$name]);
    dpm($lock->getRequestId(), _lock_id());
    if ($lock && $lock->getRequestId() == _lock_id()) {
      dpm("Releasing?");
      $lock->release();
    }
  }
}

/**
 * Release all previously acquired locks.
 */
function lock_release_all($lock_id = NULL) {
  global $locks;

  foreach ($locks as $lock) {
    unset($locks[$name]);
    if ($lock->getRequestId() == $lock_id) {
      $lock->release();
    }
  }
}
