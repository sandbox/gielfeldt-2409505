<?php
/**
 * @file
 * Queue based event handler lock item for Ultimate Lock.
 */

/**
 * Class for a lock item handling events via queues.
 */
class UltimateLockItemQueue extends UltimateLockItemBase {
  /**
   * Add unlock handler to lock.
   */
  public function addEventHandler($event, $callback, $args = array()) {
    $queue = DrupalQueue::get("ultimate_lock:$event:$this->id");
    $queue->createItem(array($callback, $args));
  }

  /**
   * Execute attached unlock handlers.
   */
  static protected function executeEventHandlers($id, $event) {
    $queue = DrupalQueue::get("ultimate_lock:$event:$id");
    while ($item = $queue->claimItem()) {
      list($callback, $args) = $item->data;
      static::invokeCallback($callback, $args);
      $queue->deleteItem($item);
    }
  }
}
