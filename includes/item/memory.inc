<?php
/**
 * @file
 * Memory based event handler lock item for Ultimate Lock.
 */

/**
 * Class for a lock item handling events via internal stack.
 */
class UltimateLockItemMemory extends UltimateLockItemBase {
  /**
   * Events to execute.
   *
   * @var array
   */
  static protected $events = array();

  /**
   * Add unlock handler to lock.
   */
  public function addEventHandler($event, $callback, $args = array()) {
    self::$events[$this->id][$event][] = array($callback, $args);
  }

  /**
   * Execute attached unlock handlers.
   */
  static protected function executeEventHandlers($id, $event) {
    if (!empty(self::$events[$id][$event])) {
      while ($data = array_shift(self::$events[$id][$event])) {
        list($callback, $args) = $data;
        static::invokeCallback($callback, $args);
      }
    }
  }
}
