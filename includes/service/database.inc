<?php
/**
 * @file
 * Database lock backend for Ultimate Lock.
 */

/**
 * An UltimateLock service using database as storage.
 */
class UltimateLockServiceDatabase extends UltimateLockServiceBase {
  /**
   * Initialize object.
   */
  public function __construct(array $options) {
    parent::__construct($options);
    $this->options += array('target' => NULL);
  }

  /**
   * Insert a lock.
   */
  public function insert(UltimateLockItemInterface $lock) {
    // Now we try to acquire the lock.
    try {
      $id = db_insert('ultimate_lock', array('target' => $this->options['target']))
        ->fields(array(
          'name' => $lock->getName(),
          'current' => 0,
          'expire' => $lock->getExpire(),
          'request_id' => $this->getRequestId(),
          'item' => get_class($lock),
        ))
        ->execute();
      return $id ? $id : FALSE;
    }
    catch (PDOException $e) {
      return FALSE;
    }
  }

  /**
   * Update an existing lock.
   */
  public function update(UltimateLockItemInterface $lock, $request_id = NULL) {
    $query = db_update('ultimate_lock', array('target' => $this->options['target']))
      ->fields(array(
        'expire' => $lock->getExpire(),
        'request_id' => $lock->getRequestId(),
      ))
      ->condition('lid', $lock->getId())
      ->condition('current', 0);

    if ($request_id) {
      $query->condition('request_id', $request_id);
    }
    return $query->execute();
  }

  /**
   * Release a lock.
   */
  public function delete($id, $expire = NULL, $request_id = NULL) {
    $query = db_update('ultimate_lock', array('target' => $this->options['target']))
      ->expression('current', 'lid')
      ->condition('lid', $id)
      ->condition('current', 0);
    if ($expire) {
      $query->condition('expire', $expire, '<');
    }
    if ($request_id) {
      $query->condition('request_id', $request_id);
    }
    return $query->execute();
  }

  /**
   * Load a lock.
   */
  public function fetch($name) {
    $lock = db_select('ultimate_lock', 'l', array('target' => $this->options['target']))
      ->fields('l', array('lid', 'request_id', 'expire', 'item'))
      ->condition('name', $name)
      ->condition('current', 0)
      ->execute()
      ->fetchObject();
    return !$lock ? FALSE : array(
      'id' => $lock->lid,
      'request_id' => $lock->request_id,
      'expire' => $lock->expire,
      'item' => $lock->item,
    );
  }

  /**
   * Garbage collect.
   *
   * Release expired locks.
   */
  public function garbageCollect() {
    $time = microtime(TRUE);
    do {
      $names = db_select('ultimate_lock', 'l', array('target' => $this->options['target']))
        ->fields('l', array('name'))
        ->condition('l.current', 0)
        ->condition('l.expire', $time, '<')
        ->range(0, 10)
        ->execute()
        ->fetchCol();

      foreach ($names as $name) {
        $lock = $this->load($name);
        if ($lock && $lock->getExpire() < $time) {
          $lock->release();
        }
      }
    } while ($names);
  }

  /**
   * Clean up the database by removed expired locks.
   */
  public function cleanup() {
    // Clean up database: remove the old locks.
    do {
      $query = db_select('ultimate_lock', 'l', array('target' => $this->options['target']))
        ->fields('l', array('lid'))
        ->range(0, 10);
      $query->where('l.current = l.lid');
      $ids = $query->execute()
        ->fetchCol();

      if ($ids) {
        db_delete('ultimate_lock', array('target' => $this->options['target']))
          ->condition('lid', $ids)
          ->execute();
      }
    } while ($ids);
  }
}
