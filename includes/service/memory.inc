<?php
/**
 * @file
 * Memory classes for Ultimate Lock.
 */

/**
 * An UltimateLock service using memory as storage.
 */
class UltimateLockServiceMemory extends UltimateLockServiceBase {
  static protected $locks = array();
  static protected $lock_ids = array();

  /**
   * Insert a lock.
   */
  public function insert(UltimateLockItemInterface $lock) {
    // Now we try to acquire the lock.
    $name = $lock->getName();
    if (empty(static::$lock_ids[$name])) {
      $id = uniqid();
      static::$lock_ids[$name] = $id;
      static::$locks[$id] = array(
        'id' => $id,
        'name' => $lock->getName(),
        'expire' => $lock->getExpire(),
        'request_id' => $lock->getRequestId(),
        'item' => get_class($lock),
      );
      return $id;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Update an existing lock.
   */
  public function update(UltimateLockItemInterface $lock, $request_id = NULL) {
    $id = $lock->getId();
    if (empty(static::$locks[$id])) {
      return FALSE;
    }

    if ($request_id && $request_id !== static::$locks[$id]['request_id']) {
      return FALSE;
    }
    static::$locks[$id]['request_id'] = $lock->getRequestId();

    static::$locks[$id]['expire'] = $lock->getExpire();
    return TRUE;
  }

  /**
   * Release a lock.
   */
  public function delete($id, $expire = NULL, $request_id = NULL) {
    if (empty(static::$locks[$id])) {
      return FALSE;
    }
    if ($expire && static::$locks[$id]['expire'] < $expire) {
      return FALSE;
    }
    if ($request_id && static::$locks[$id]['request_id'] !== $request_id) {
      return FALSE;
    }
    $name = static::$locks[$id]['name'];
    unset(static::$locks[$id], static::$lock_ids[$name]);
    return TRUE;
  }

  /**
   * Load a lock.
   */
  public function fetch($name) {
    if (empty(static::$lock_ids[$name])) {
      return FALSE;
    }
    $lock = static::$locks[static::$lock_ids[$name]];
    return array(
      'id' => $lock['id'],
      'request_id' => $lock['request_id'],
      'expire' => $lock['expire'],
      'item' => $lock['item'],
    );
  }

  /**
   * Garbage collect.
   *
   * Release expired locks.
   */
  public function garbageCollect() {
    $time = microtime(TRUE);
    foreach (static::$lock_ids as $name => $id) {
      $lock = $this->load($name);
      if ($lock && $lock->getId() === $id && $lock->getExpire() < $time) {
        $lock->release();
      }
    }
  }

  /**
   * Not applicable for per request memory locks.
   */
  public function cleanup() {
  }
}
