<?php
/**
 * @file
 * Database lock backend for Ultimate Lock.
 */

require_once DRUPAL_ROOT . '/' . variable_get('ultimate_lock_dmemcache_inc', 'sites/all/modules/contrib/memcache/dmemcache.inc');

/**
 * An UltimateLock service using database as storage.
 */
class UltimateLockServiceMemcache extends UltimateLockServiceBase {
  /**
   * Initialize object.
   */
  public function __construct(array $options) {
    parent::__construct($options);
  }

  /**
   *  Increment an item in the memcache.
   *
   * @param $key The string with which you will retrieve this item later.
   * @param $inc The item to increment with.
   * @param $init The initial value, if the item doesn't exist.
   * @param $exp Parameter expire is expiration time in seconds. If it's 0, the
   *   item never expires (but memcached server doesn't guarantee this item to be
   *   stored all the time, it could be deleted from the cache to make place for
   *   other items).
   * @param $bin The name of the Drupal subsystem that is making this call.
   *   Examples could be 'cache', 'alias', 'taxonomy term' etc. It is possible
   *   to map different $bin values to different memcache servers.
   * @param $mc Optionally pass in the memcache object.  Normally this value is
   *   determined automatically based on the bin the object is being stored to.
   * @param $flag If using the older memcache PECL extension as opposed to the
   *   newer memcached PECL extension, the MEMCACHE_COMPRESSED flag can be set
   *   to use zlib to store a compressed copy of the item.  This flag option is
   *   completely ignored when using the newer memcached PECL extension.
   *
   * @return bool
   */
  public function dmemcache_inc($key, $inc = 1, $init = 0, $exp = 0, $bin = 'cache', $mc = NULL, $flag = FALSE) {
    global $_memcache_statistics;
    $full_key = dmemcache_key($key, $bin);
    if (dmemcache_collect_stats()) {
      $_memcache_statistics[] = array('inc', $bin, $full_key, '');
    }
    if ($mc || ($mc = dmemcache_object($bin))) {
      if ($mc instanceof Memcached) {
        return $mc->increment($full_key, $inc, $init, $exp);
      }
      else {
        $result = $mc->increment($full_key, $inc);
        if ($result) {
          return $result;
        }
        if (dmemcache_add($key, $init, $exp, $bin, $mc, $flag)) {
          return $init;
        }
        return $mc->increment($full_key, $inc);
      }
    }
    return FALSE;
  }

  protected function getNextId() {
    $id = $this->dmemcache_inc('ultimate_lock::serial', 1, 1, 0, 'semaphore');
    return (string) $id;
  }

  /**
   * Insert a lock.
   */
  public function insert(UltimateLockItemInterface $lock) {
    $id = $this->getNextId();
    if (dmemcache_add($lock->getName(), $id, $lock->getExpire(), 'semaphore')) {
      $key = 'ultimate_lock::' . $id;
      if (dmemcache_set($key, array(
        'name' => $lock->getName(),
        'expire' => $lock->getExpire(),
        'request_id' => $lock->getRequestId(),
        'item' => get_class($lock),
      ), $lock->getExpire(), 'semaphore')) {
        return $id;
      }
    }
    return FALSE;
  }

  /**
   * Update an existing lock.
   */
  public function update(UltimateLockItemInterface $lock, $request_id = NULL) {
    $key = 'ultimate_lock::' . $lock->getId();
    $key_update = $lock->getName() . '::update';

    $current_lock = dmemcache_get($key, 'semaphore');
    if (!$current_lock) {
      return FALSE;
    }

    if ($request_id && $request_id !== $current_lock['request_id']) {
      return FALSE;
    }

    if (!dmemcache_add($key_update, 1, time() + 60, 'semaphore')) {
      return FALSE;
    }
    dmemcache_set($lock->getName(), $lock->getId(), $lock->getExpire(), 'semaphore');
    dmemcache_delete($key_update, 'semaphore');

    if (dmemcache_set($key, array(
      'expire' => $lock->getExpire(),
      'request_id' => $lock->getRequestId(),
    ) + $current_lock, $lock->getExpire(), 'semaphore')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Release a lock.
   */
  public function delete($id, $expire = NULL, $request_id = NULL) {
    $key = 'ultimate_lock::' . $id;
    $current_lock = dmemcache_get($key, 'semaphore');
    if (!$current_lock) {
      return FALSE;
    }
    $key_update = $current_lock['name'] . '::update';
    if (!dmemcache_add($key_update, 1, time() + 60, 'semaphore')) {
      return FALSE;
    }

    dmemcache_delete($key, 'semaphore');
    $current_id = dmemcache_get($current_lock['name'], 'semaphore');
    if ($current_id === $id) {
      dmemcache_delete($current_lock['name'], 'semaphore');
    }
    dmemcache_delete($key_update, 'semaphore');
    return TRUE;
  }

  /**
   * Load a lock.
   */
  public function fetch($name) {
    $id = dmemcache_get($name, 'semaphore');
    $result = $id ? dmemcache_get('ultimate_lock::' . $id, 'semaphore') : FALSE;
    return !$result ? FALSE : array(
      'id' => $id,
      'request_id' => $result['request_id'],
      'expire' => $result['expire'],
      'item' => $result['item'],
    );
  }
}
